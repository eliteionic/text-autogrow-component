import { Directive, Renderer2, HostListener } from '@angular/core';
import { DomController } from '@ionic/angular';

@Directive({
  selector: '[appAutogrow]',
})
export class AutogrowDirective {
  constructor(private renderer: Renderer2, private domCtrl: DomController) {}

  @HostListener('input', ['$event.target'])
  resize(textarea: HTMLTextAreaElement) {
    let newHeight: number;

    this.domCtrl.read(() => {
      newHeight = textarea.scrollHeight;
    });

    this.domCtrl.write(() => {
      this.renderer.setStyle(textarea, 'height', `${newHeight}px`);
    });
  }
}
